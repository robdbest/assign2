// var date = new Date();
// console.log("Year: " + date.getFullYear());
// console.log("Month: " + date.getMonth());
// console.log("Day: " + date.getDate());
// console.log("Hour: " + date.getHours());
// console.log("Minute: " + date.getMinutes());

// 1.) Create a function, showCurrentTime, to display message with information about the current time. 
//Call the function to replace the first paragraph in Part 1 of page1.html. 

var idbol = document.getElementById('date-display');
idbol.textContent = showCurrentTime();

function showCurrentTime() {
    var dateMessage = 'Current Date and Time On Your Computer : ';
    var today = new Date();
    var dateString;
    var days;
    var year;

    dateString = today.getMonth() + 1;
    if ( dateString < 10 ) {
        dateString = '0' + dateString;
    }
    dateString = dateString + '/';

    days = today.getDate();
    if ( days < 10) {
        days = '0' + days;
    }
    dateString = dateString + days + '/' + today.getFullYear() + ' at ' + today.getHours() + ':' + today.getMinutes ();
    
    dateMessage = dateMessage + dateString;
   
    return dateMessage;
}

console.log(showCurrentTime());



/* 2.) Create 2 product objects (product1 and product2) using literal notation; each product has 4 data members 
and 1 method. The 4 data members are: productNumber, description, price, and fee, and a method totalAmount, 
which returns the sum of price and fee of the product.*/

var product1 = {
    productNumber: 1,
    description: 'product1', 
    price: 10, 
    fee: 1,
    totalAmount: function() {
        return this.price + this.fee;
    }
};
var product2 = {
    productNumber: 2,
    description: 'product2', 
    price: 20, 
    fee: 2,
    totalAmount: function() {
        return this.price + this.fee;
    }
};











/* 3.) Use the constructor notation (i.e. the function should be called Product) 
to create 4 product objects which have the same components (productNumber, 
description, price, fee, and totalAmount function). 
Make sure some products charge a fee and others don't (i.e. the fee is zero).*/

function Product(productNumber, description, price, fee) {
    this.productNumber = productNumber;
    this.description = description;
    this.price = price;
    this.fee = fee;

    this.totalAmount = function() {
        return this.price + this.fee;  
    };
}
/* 4.) Use the constructor notation (i.e. the function should be called Product) 
to create 4 product objects which have the same components 
(productNumber, description, price, fee, and totalAmount function). 
Make sure some products charge a fee and others don't (i.e. the fee is zero). */

var product3 = new Product(010, 'product3', 200, 20);
var product4 = new Product(4, 'product4', 40, 4);
var product5 = new Product(5, 'product5', 50, 0);
var product6 = new Product(6, 'product6', 60, 0);

// 5.) Add all 6 product objects to an array named products.

var products = [product1, product2, product3, product4, product5, product6];






/* 6.) Create a function, productionTable, to present the information stored in the products 
array in an HTML table. Call the function to replace the HTML table in Part 2 of page1.html.*/

function productionTable() {
   
    var htmlString = '<tr><th> productNumber </th><th> description </th><th> price </th><th> fee </th><th> total </th></tr>'; 
    var rowstring;

    for (var i = 0; i < products.length; i++) {
        console.log(products[i].description);
        //productNumber, description, price, fee
        rowstring = '<tr><td>' + products[i].productNumber + '</td><td>' + products[i].description + 
            '</td><td>' + products[i].price + '</td><td>' + products[i].fee + '</td><td>' + products[i].totalAmount() + '</td></tr>';

        htmlString = htmlString + rowstring;
        console.log(htmlString);
    }
    var idTable = document.getElementById('mytable');
    idTable.innerHTML = htmlString;
}

productionTable();  //Note calling function productionTableString below









/* 7.) CHALLENGE: create a productionTableString function that takes one parameter:  the array of products. 
The function should return a string that represents an HTML table filled with all products in the products array.*/

function productionTableString(prod) {
    var htmlString = '<tr><th> productNumber </th><th> description </th><th> price </th><th> fee </th><th> total </th></tr>'; 
    var rowstring;

    for (var i = 0; i < prod.length; i++) {
        console.log(prod[i].description);
        //productNumber, description, price, fee
        rowstring = '<tr><td>' + prod[i].productNumber + '</td><td>' + prod[i].description + 
            '</td><td>' + prod[i].price + '</td><td>' + prod[i].fee + '</td><td>' + prod[i].totalAmount() + '</td></tr>';
        htmlString = htmlString + rowstring;
        console.log(htmlString);  
    }

    return htmlString;
}

var idTable = document.getElementById('mytable');
    //idTable.innerHTML = productionTableString(products);
    





