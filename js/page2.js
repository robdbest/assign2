/*Step 3 solution should modify ONLY  page2.js.
2.) Create class Product that contains the same data members: 
productNumber, description, price, fee, and a method totalAmount 
(the method returns sum of the price and the fee). */


function Product(productNumber, description, price, fee) {
  this.productNumber = productNumber;
  this.description = description;
  this.price = price;
  this.fee = fee;

  this.totalAmount = function() {
      return this.price + this.fee;  
  };
}

// 3.) Use the class to create 5 products and store them in an array products.

var product1 = new Product(010, 'product1', 30, 3);
var product2 = new Product(2, 'product2', 40, 4);
var product3 = new Product(3, 'product3', 50, 0);
var product4 = new Product(4, 'product4', 60, 0);
var product5 = new Product(5, 'product5', 60, 0);



//Add all 6 product objects to an array named products.

var products = [product1, product2, product3, product4, product5];



/* 4.) Create a function, printProductDescriptions. The function takes one parameter (the products array) 
and returns a string representing  descriptions of all products as an HTML unordered list.  
Replace the unordered list in Part 4 of page2.html with the string returned from the printProductDescriptions 
function. */


function printProductDescriptions(productArray) {
    var htmlString = '<li>' + productArray[0].description + '</li><li>' + productArray[1].description +  '</li><li>' + productArray[2].description +  
      '</li><li>'+ productArray[3].description +  '</li><li>' + productArray[4].description +  '</li>'; 
  
    console.log(productArray[0].description);
    return htmlString;
}

    var idunorderedList = document.getElementById('ulist');
    idunorderedList.innerHTML = printProductDescriptions(products);




/* 5.) Create a function, printProductPrices. The function takes one parameter (the products array) 
and returns a string representing total prices of all products (both price and fee) as an HTML ordered list. 
Replace the ordered list in Part 4 of page2.html with the string returned from the printProductPrices function. */

function printProductPrices(productArray) {
    var htmlString = '<li>$' + productArray[0].totalAmount() + '</li><li>$' + productArray[1].totalAmount() +  '</li><li>$' + productArray[2].totalAmount() +  
      '</li><li>$'+ productArray[3].totalAmount() +  '</li><li>$' + productArray[4].totalAmount() +  '</li>'; 

    return htmlString;
}

var idOrderedList = document.getElementById('olist');
idOrderedList.innerHTML = printProductPrices(products);
